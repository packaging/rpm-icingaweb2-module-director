# Icinga Director | (c) 2017-2019 Icinga Development Team | GPLv2+

%global revision 1
%global module_name director
%global module_version 1.6.2

%global icingaweb_min_version 2.6.0

%global version_ipl 0.2.1
%global version_incubator 0.2.0
%global version_reactbundle 0.5.1

Name:           icingaweb2-module-%{module_name}
Version:        %{module_version}
Release:        %{revision}%{?dist}
Summary:        Icinga Director module for Icinga Web 2
Group:          Applications/System
License:        GPLv2+
URL:            https://icinga.com
Source0:        https://github.com/Icinga/icingaweb2-module-%{module_name}/archive/v%{version}.tar.gz
Source1:        %{service_name}.service
#/icingaweb2-module-%{module_name}-%{version}.tar.gz
BuildArch:      noarch

%global basedir %{_datadir}/icingaweb2/modules/%{module_name}
%global service_name icinga-%{module_name}

%if "%{_vendor}" == "suse"
%global service_user wwwrun
%else # suse
%global service_user apache
%endif # suse

BuildRequires:  systemd-devel
Requires:       systemd

Requires:       icingaweb2 >= %{icingaweb_min_version}
Requires:       php-Icinga >= %{icingaweb_min_version}

Requires:       icingaweb2-module-ipl >= %{version_ipl}
Requires:       icingaweb2-module-incubator >= %{version_incubator}
Requires:       icingaweb2-module-reactbundle >= %{version_reactbundle}

%description
Icinga Director has been designed to make Icinga 2 configuration handling easy.
It tries to target two main audiences:

* Users with the desire to completely automate their datacenter
* Sysops willing to grant their "point & click" users a lot of flexibility
* What makes Icinga Director so special is the fact that it tries to target
  both of them at once

%prep
%setup -q
#-n icingaweb2-module-%{module_name}-%{version}

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}%{basedir}

cp -r * %{buildroot}%{basedir}

install -d %{buildroot}%{_unitdir}
install -m 0644 %{SOURCE1} %{buildroot}%{_unitdir}/%{service_name}.service

# Replace user in service unit
sed -i -e 's~^User=.*~User=%{service_user}~' %{buildroot}%{_unitdir}/%{service_name}.service

%if "%{_vendor}" == "suse"
install -d %{buildroot}%{_sbindir}
ln -sf /usr/sbin/service %{buildroot}%{_sbindir}/rc%{service_name}
%endif # suse

%clean
rm -rf %{buildroot}

%pre
%if "%{_vendor}" == "suse"
  %service_add_pre %{service_name}.service
%endif # suse

exit 0

%post
set -e

# Only for fresh installations
if [ $1 == 1 ]; then
    if [ ! -d /etc/icingaweb2/enabledModules ]; then
        mkdir /etc/icingaweb2/enabledModules
        chmod g=rwx,o= /etc/icingaweb2/enabledModules
    fi

    echo "Enabling icingaweb2 module '%{module_name}'"
    ln -svf /usr/share/icingaweb2/modules/%{module_name} /etc/icingaweb2/enabledModules/%{module_name}
fi

%if "%{_vendor}" == "suse"
  %service_add_post %{service_name}.service
%else # suse
%systemd_post %{service_name}.service
%endif # suse

exit 0

%preun
set -e

%if "%{_vendor}" == "suse"
  %service_del_preun %{service_name}.service
%else # suse
  %systemd_preun %{service_name}.service
%endif # suse

# Only for removal
if [ $1 == 0 ]; then
    echo "Disabling icingaweb2 module '%{module_name}'"
    rm -f /etc/icingaweb2/enabledModules/%{module_name}
fi

exit 0

%postun
set -e

%if "%{_vendor}" == "suse"
  %service_del_postun %{service_name}.service
%else # suse
  %systemd_postun_with_restart %{service_name}.service
%endif # suse

exit 0

%files
%doc README.md LICENSE

%defattr(-,root,root)
%{basedir}

%{_unitdir}/%{service_name}.service

%if "%{_vendor}" == "suse"
%{_sbindir}/rc%{service_name}
%endif # suse

%changelog
* Mon Sep 09 2019 Markus Frosch <markus.frosch@icinga.com> - 1.6.2-1
- Release version 1.6.2-1

* Fri Aug 30 2019 Markus Frosch <markus.frosch@icinga.com> - 1.6.2-1
- Add icinga-director service

* Thu May 02 2019 Markus Frosch <markus.frosch@icinga.com> - 1.6.2-1
- Update packaging
- Update to 1.6.2

* Wed Mar 28 2018 Markus Frosch <markus.frosch@icinga.com> - 1.4.3-1
- Initial package version
